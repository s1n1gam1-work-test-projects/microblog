export const state = () => ({
    menu:[
        {
            title:'Главная',
            to:'/'
        },
        {
            title:'Пользователи',
            to:'/users'
        },
        {
            title:'Альбомы',
            to:'/albums'
        },
        {
            title:'Фотографии',
            to:'/photos'
        },
        {
            title:'Все посты',
            to:'/posts'
        },
    ],
})
