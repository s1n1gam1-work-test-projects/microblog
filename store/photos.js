import PhotoService from '@/services/PhotoService'

export const state = () => ({
    photos:[],
    photo:{},
})

export const mutations = {
    SET_PHOTOS(state,photos){
        state.photos = photos
    },

    SET_PHOTO(photo){
        state.photo = photo
    }
}

export const actions = {
    fetchPhotos ({commit}){
        return PhotoService.getPhotos().then((response)=>{
            let photos = response.data.filter((item,index)=>index<100)
            console.log(photos)
            commit('SET_PHOTOS',photos)
        })
    },

    fetchPhoto({commit}, id){
        return PhotoService.getPhoto(id).then((response)=>{
            commit('SET_PHOTO',response.data)
        })
    }
}