import AlbumService from '@/services/AlbumService'

export const state = () => ({
    albums:[],
    album:{},
})

export const mutations = {
    SET_ALBUMS(state,albums){
        state.albums = albums
    },

    SET_ALBUM(album){
        state.album = album
    }
}

export const actions = {
    fetchAlbums ({commit}){
        return AlbumService.getAlbums().then((response)=>{
            let albums = response.data.filter((item,index)=>index<100)
            commit('SET_ALBUMS',albums)
        })
    },

    fetchAlbum({commit}, id){
        return AlbumService.getAlbum(id).then((response)=>{
            commit('SET_ALBUM',response.data)
        })
    }
}