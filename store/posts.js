import PostService from '@/services/PostService'

export const state = () => ({
    posts:[],
    post:{}
})

export const mutations = {
    SET_POSTS(state,posts){
        state.posts = posts
    },

    SET_POST(post){
        state.post = post
    }
}

export const actions = {
    fetchPosts({commit}){
        return PostService.getPosts().then((response)=>{
            let posts = response.data.filter((item,index)=>index<100)
            commit('SET_POSTS',posts)
        })
    },

    fetchPost({commit}, id){
        return PostService.getPost(id).then((response)=>{
            commit('SET_POST',response.data)
        })
    }
}