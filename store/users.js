import UserService from '@/services/UserService'

export const state = () => ({
    users:[],
    user:{},
})

export const mutations = {
    SET_USERS(state,users){
        state.users = users
    },

    SET_USER(user){
        state.user = user
    }
}

export const actions = {
    fetchUsers ({commit}){
        return UserService.getUsers().then((response)=>{
            let users = response.data.filter((item,index)=>index<100)
            commit('SET_USERS',users)
        })
    },

    fetchUser({commit}, id){
        return UserService.getUser(id).then((response)=>{
            commit('SET_USER',response.data)
        })
    }
}