import apiClient from './apiClient'

export default {
    getPosts(){
        return apiClient.get('/posts')
    },
    getPost(id){
        return apiClient.get('/posts/'+id)
    }
}