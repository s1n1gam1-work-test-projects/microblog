import axios from 'axios'

const apiClient = axios.create({
	baseURL: `https://picsum.photos`,
	withCredentials: false, 
	headers: {
	Accept: 'application/json',
		'Content-Type': 'application/json'
	}
})

export default {
    getPhotos(){
        return apiClient.get('/v2/list')
    },
    getPhoto(id){
        return apiClient.get('/id/'+id+'/info')
    }
}