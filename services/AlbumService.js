import apiClient from './apiClient'

export default {
    getAlbums(){
        return apiClient.get('/albums')
    },
    getAlbum(id){
        return apiClient.get('/albums/'+id)
    }
}